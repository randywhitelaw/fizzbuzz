﻿using System;
using FizzBuzz.Printers;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new PrinterFactory();

            for (var i = 1; i <= 100; i++)
            {
                var printer = factory.GetPrinter(i);

                Console.WriteLine(printer.Print(i));
            }

            Console.ReadKey();
        }
    }
}
