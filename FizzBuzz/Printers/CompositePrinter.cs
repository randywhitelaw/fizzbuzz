﻿using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Printers
{
    sealed class CompositePrinter : IPrinter
    {
        private readonly IEnumerable<IPrinter> _printers;

        public CompositePrinter(IEnumerable<IPrinter> printers)
        {
            _printers = printers;
        }

        public string Print(int input)
        {
            var sb = new StringBuilder();

            foreach (var printer in _printers)
            {
                sb.Append(printer.Print(input));
            }

            return sb.ToString();
        }
    }
}
