﻿namespace FizzBuzz.Printers
{
    sealed class WordPrinter : IPrinter
    {
        private readonly string _word;

        public WordPrinter(string word)
        {
            _word = word;
        }

        public string Print(int input)
        {
            return _word;
        }
    }
}
