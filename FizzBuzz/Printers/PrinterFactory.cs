﻿using System.Collections.Generic;

namespace FizzBuzz.Printers
{
    sealed class PrinterFactory
    {
        private readonly IPrinter _fizzPrinter = new WordPrinter("Fizz");
        private readonly IPrinter _buzzPrinter = new WordPrinter("Buzz");
        private readonly IPrinter _echoPrinter = new EchoPrinter();
        private readonly IPrinter _fizzBuzzPrinter;

        public PrinterFactory()
        {
            var printers = new List<IPrinter>
            {
                _fizzPrinter,
                _buzzPrinter,
            };

            _fizzBuzzPrinter = new CompositePrinter(printers);
        }

        public IPrinter GetPrinter(int input)
        {
            if (input % 5 == 0 && input % 3 == 0)
            {
                return _fizzBuzzPrinter;
            }

            if (input % 3 == 0)
            {
                return _fizzPrinter;
            }

            if (input % 5 == 0)
            {
                return _buzzPrinter;
            }

            return _echoPrinter;
        }
    }
}
