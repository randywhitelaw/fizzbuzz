﻿using System.Globalization;

namespace FizzBuzz.Printers
{
    sealed class EchoPrinter : IPrinter
    {
        public string Print(int input)
        {
            return input.ToString(CultureInfo.InvariantCulture);
        }
    }
}
