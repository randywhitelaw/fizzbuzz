﻿namespace FizzBuzz.Printers
{
    interface IPrinter
    {
        string Print(int input);
    }
}
